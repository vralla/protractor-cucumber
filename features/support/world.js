const {setWorldConstructor} = require("cucumber");
/**
 * https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/world.md
 * World - created before every scenario and destroyed after scenario
 * Used to share data and state within the scneario 
 */
function World({ attach, parameters }) {
    // Enables the world to attach data ex: screenshots
    this.attach = attach;
    this.parameters = parameters;
    this.url = "";
    this.setUrl = function(url){
        this.url = url;
    }
}

//Override the default with setWorldConstructor constructor
setWorldConstructor(World);