
const {setDefaultTimeout, Before, BeforeAll, After, AfterAll, Status} = require("cucumber");
const config = require("config");
var {browser, $, element, ExpectedConditions} = require("protractor");

/**
 * This is the default timeout and you no longer need to enter for each step definition
 */
setDefaultTimeout(config.get("globalTimeout"));

/**
 * Runs before the feature is run
 */
BeforeAll(async function(){
    console.log("Running steps before the feature");
});

/**
 * Runs after the feature is run
 */
AfterAll(async function(){
    console.log("Running steps after the feature");
    browser.close();
});

/**
 * Runs before every scenario
 */
Before(async function(){
    
});

/**
 * runs after every scenario
 */
After(async function(scenario){
    /**
     * Takes screen shot on failure 
     */
    if(scenario.result.status === Status.FAILED) {
        const png = await browser.takeScreenshot();
            var decodedImage = new Buffer.alloc(png.length, png, "base64");
        return this.attach(decodedImage, "image/png");
    }
});