Feature: https://ncai.nhlbi.nih.gov/ncai/ Resources
This is to test resources menu on /ncai page
  
  Background: Go to /ncai website
    Given browse to web site "https://ncai.nhlbi.nih.gov/ncai/"

  Scenario Outline: Test resources
    When user navigates to "<resource>" resources
    Then user is taken to "<resource_page>" resources
    
    Examples:
      | resource | resource_page |
      | NHLBI | NHLBI |
      | partners | partners |
      | B-BIC | B-BIC |
      | NCAI-CC | NCAI-CC |
      | UC CAI | UC CAI |
