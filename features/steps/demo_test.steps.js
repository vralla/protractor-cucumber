var config = require("config");
var chai = require("chai").use(require("chai-as-promised"));
var expect = chai.expect;
const { Given, When, Then } = require('cucumber');
var {browser, $, element, ExpectedConditions} = require("protractor");
// const globalTimeout = 8 * config.get("globalTimeout"); // We have global time out set in features/support/hooks.js
const browserTimeout = config.get("browserTimeout");
browser.ignoreSynchronization = true;

// NCAI steps
Given(/^browse to web site "([^"]*)"$/, {timeout: 5 * 30000}, async function(url) {
    await browser.manage().window().maximize();
    await browser.get(url);
    // 
    this.setUrl(url);
    await browser.wait(ExpectedConditions.urlIs(url), (10 * browserTimeout));
});

When(/^user navigates to "([^"]*)" resources$/, async function (arg) {
    url = this.url;
    console.log(`Validating ${arg} under "Resources" menu located on ${url}`);
    await element(By.xpath("//a[.='Resources']")).click();
    switch(arg) {
        case "NHLBI":
            return await element(By.css('[href="/ncai/resources/nhlbi11"]')).click();
            break;
        case "partners":
            return await element(By.css('[href="/ncai/resources/other"]')).click();
            break;
        case "B-BIC":
            return await element(By.css('[href="/ncai/resources/boston"]')).click();
            break;   
        case "NCAI-CC":
            return await element(By.css('[href="/ncai/resources/cleveland"]')).click();
            break;  
        case "UC CAI":
            return await element(By.css('[href="/ncai/resources/uc"]')).click();
            break;                 
      }
});

Then(/^user is taken to "([^"]*)" resources$/, async function (arg) {
    switch(arg) {
        case "NHLBI":
            expect(await element(By.xpath("//h1[.='NIH/NHLBI Resources']")).isDisplayed()).to.be.true;
            break;
        case "partners":
            expect(await element(By.xpath("//h1[.='Partner Resources']")).isDisplayed()).to.be.true;
            break;
        case "B-BIC":
            expect(await element(By.xpath("//h1[.='B-BIC Resources']")).isDisplayed()).to.be.true;
            break;  
        case "NCAI-CC":
            expect(await element(By.xpath("//h1[.='NCAI-CC Resources']")).isDisplayed()).to.be.true;
            break;
        case "UC CAI":
            expect(await element(By.xpath("//h1[.='UC CAI Resources']")).isDisplayed()).to.be.true;
            break;  
    }
});


Then('close the browser', async function () {
    await browser.close();
});

