# README #

This README will help you get started with protractor-cucumber-framework.

### What is this repository for? ###

* This is a POC for BDD tests using protractor-cucumber-framework.

### Pre-requesites
* NodeJS and NPM. https://nodejs.org/en/ Nodejs is JavaScript run-time environment. Npm is JavaScript’s package manager, equivalent to Maven for Java, NuGet for C#, etc. NPM comes built-in with nodejs.
* Visual Studio Code (IDE). https://code.visualstudio.com

### How to start selenium webdriver 
* npm install -g webdriver-manager
* Update webdriver-manager
* webdriver-manager start

### How do I run BDD tests? ###

* checkout this repo by cloning it. 'git clone https://vralla@bitbucket.org/vralla/protractor-cucumber.git'
* Go to protractor-cucumber directory. 'cd protractor-cucumber'.
* Install npm packages. 'npm install' 
* To run the feature files: 'node index.js'.


### Contribution guidelines ###

* Pull a new branch.
* Make your changes and submit your merge request for review. 
* After code review, the reviewer merges your branch

### Who do I talk to? ###

* Vasudha Ralla
