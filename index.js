var reporter = require('cucumber-html-reporter');
var shell = require('shelljs');
var os = require("os");
var config = require("config");
/**
 * Options for HTML report
 */
var options = {
    theme: "bootstrap",
    jsonFile: config.get("report.dir") + config.get("report.results"),
    output: `${config.get("report.dir")}/cucumber_report${(new Date).getTime()}.html`, //back quotes
    reportSuiteAsScenarios: true,
    launchReport: true,
    metadata: {
        "Browser": "Chrome",
        "Platform": os.type(),
        "Parallel": "Scenarios",
        "Executed": "Remote",

    }
};

/** 
 * Run protractor and generate HTML report
 */
async function runProtractor(){
    await shell.exec("npm run protractor"); // run as a child process
    await reporter.generate(options); // generates the report after protractor runs
}

runProtractor();