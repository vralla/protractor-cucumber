var config = require("config");
exports.config = {
    /**
     * Selenium address
     */
    seleniumAddress: "http://localhost:4444/wd/hub",
    framework: 'custom', //custom, we will use protractor-cucumber
    frameworkPath: require.resolve("protractor-cucumber-framework"),
    
    /*
    capabilities: {
        browserName: "chrome",
        chromeOptions: {
            args: ["--headless", "--disable-gpu", "window-size=800,600" ] //headless chrome
        }
    },
    */
    multiCapabilities: [{
        'browserName': 'firefox',
        'moz:firefoxOptions': {
            //'args': ['--headless']
        }
    }, {
        'browserName': 'chrome',
        'chromeOptions': {
            //'args': ["--headless", "--disable-gpu", "window-size=800,600" ] //headless chrome
        }
    }],
    maxSessions: 2,
    ignoreUncaughtExceptions: "true", //ignore uncaught exceptions
    ignoreSynchronization: true, //runs on non-angular applications
    
    specs: ["features/*.feature"],
    
    cucumberOpts: {
        require: ["features/steps/*.js", "features/support/*.js"],
        format: `json:${config.get("report.dir") + config.get("report.results")}`,
        strict: true

    },

    plugins: [{
        package: 'protractor-multiple-cucumber-html-reporter-plugin',
        options: {
            automaticallyGenerateReport: true,
            removeExistingJsonReportFile: true
        }
    }]
}
